﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Just_you_wait
{
    class Spawner
    {
        enum SpawnerPosition
        {
            UpperLeft,
            UpperRight,
            DownLeft,
            DownRight
        }
        public int X { get; set; }
        public int Y { get; set; }
    }
}
