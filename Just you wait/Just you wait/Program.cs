﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Just_you_wait
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form myForm = new Form1();
            GameModel myGameModel = new GameModel();

            //myForm.SizeChanged += myGameModel.GameView_Resize;
            //myForm.KeyDown += myGameModel.GameModel_KeyPress;

            myGameModel.StartGame();

            Application.Run(myForm);



        }
    }
}
