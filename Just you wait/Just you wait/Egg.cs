﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Just_you_wait
{
    class Egg
    {
        public int EggX {  get; set;}
        public int EggY { get; set; }


        private int _eggSpeed;
        public int EggSpeed { get => _eggSpeed; 
            
            set { 
                if(value<0)
                {
                    _eggSpeed = 0;
                }
                else
                {
                    if(value>10)
                    {
                        _eggSpeed = 10; 
                    }
                    else
                    {
                        _eggSpeed = value;
                    }
                }
            } 
        }

        public void ChangeEggPos()
        {
            if (EggY > 7)
            {
                EggY -= 1; 
            }

        }

    }
}
